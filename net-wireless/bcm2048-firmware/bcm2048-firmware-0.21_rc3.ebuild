# Copyright 2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

EAPI=4

inherit eutils

DESCRIPTION="Binary Nokia N900 bcm2048 bluetooth firmware"
LICENSE="Nokia"
HOMEPAGE="http://maemo.org"

SRC_URI=""
KEYWORDS="arm"
SLOT="0"
RESTRICT="mirror installsources bindist"

IUSE=""
RDEPEND=""
DEPEND=""

S="${WORKDIR}"

src_unpack() {
	local uri myA="bt-firmware_0.21-rc3+0m5_all.deb"
	local myDA="${DISTDIR}/${myA}"
	if ! [ -e "$myDA" ]; then
		addwrite /sys/devices/platform/gpio-switch
		uri="$("${FILESDIR}/n900-password" -r)"
		[ -n "$uri" ] || die 'Failed to generate password. Please figure out how to fetch bt-firmware_0.21-rc3+0m5_all.deb yourself'
		addwrite "$myDA"
		wget --auth-no-challenge "${uri}ssu/002/bt-firmware_0.21-rc3+0m5_all.deb" -O "${myDA}"
	fi
	unpack "${myA}"
	unpack ./data.tar.gz
	unpack ./usr/share/doc/bt-firmware/changelog.Debian.gz
}

src_prepare() {
	mv changelog.Debian changelog
}

src_install() {
	dodoc changelog
	insinto /lib/firmware
	doins lib/firmware/bcmfw.bin
}
