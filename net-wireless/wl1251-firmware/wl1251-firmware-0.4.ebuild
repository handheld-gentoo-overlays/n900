# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"

DESCRIPTION="Firmware for TI wl1251 wireless chipset"
HOMEPAGE="http://www.ti.com/general/docs/wtbu/wtbuproductcontent.tsp?templateId=6123&navigationId=12494&contentId=4711"
myPN="ti-${PN}"
myP="${myPN}_${PV}"
SRC_URI="https://api.pub.meego.com/public/source/CE:Adaptation:N900/${myPN}/${myP}.tar.bz2"

LICENSE="TI-236402v3"
SLOT="1"
KEYWORDS="arm"
IUSE=""

S="${WORKDIR}/${myP/_/-}"

src_compile() { :; }
