# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

EAPI="4"
K_SECURITY_UNSUPPORTED=1
K_NOSETEXTRAVERSION=1
ETYPE="sources"
K_DEBLOB_AVAILABLE="0"
inherit eutils git-2 versionator

MY_PN="kernel"
CKV="$(get_version_component_range 1-3 ${PV})"
UNIPATCH_STRICTORDER=true
inherit kernel-2
detect_version

myPKV="$(get_version_component_range 4 ${PV})"
POWERVERSION="$(get_version_component_range 5- ${PV})"
EXTRAVERSION=".${myPKV}-fremantle-power${POWERVERSION}"
KV="${CKV}${EXTRAVERSION}"
MyPR="-${PR}"
[ "${MyPR}" = '-r0' ] && MyPR=
KV_FULL="${KV}${MyPR}"
S="${WORKDIR}/linux-${KV_FULL}"

EGIT_REPO_URI='https://vcs.maemo.org/git/kernel-power'
EGIT_COMMIT='60205ed4287d54101131680bb60384ed38074908'
EGIT_SOURCEDIR="${WORKDIR}"
EGIT_NOUNPACK=1
POWER_PATCH_DIR="${EGIT_SOURCEDIR}/kernel-power-2.6.28/debian/patches/"

DESCRIPTION="Maemo 5 power kernel based on Linux"
HOMEPAGE="http://wiki.maemo.org/Kernel_Power"
SRC_URI="${KERNEL_URI}"

KEYWORDS="~arm"
IUSE="fcam"
DEPEND="${DEPEND} sys-apps/sed"

src_unpack() {
	GIT_SSL_NO_VERIFY=1 \
	git-2_src_unpack
	while read p; do
		[ "${p:0:1}" = '#' ] && continue
		UNIPATCH_LIST_DEFAULT="${UNIPATCH_LIST_DEFAULT} ${POWER_PATCH_DIR}/$p"
	done < "${POWER_PATCH_DIR}/series"
	if use fcam; then
		UNIPATCH_LIST_DEFAULT="$UNIPATCH_LIST_DEFAULT
			${FILESDIR}/${PV}_fcam-1.0.7-1.patch
		"
	fi
	UNIPATCH_LIST_DEFAULT="$UNIPATCH_LIST_DEFAULT
		${FILESDIR}/kbuild-fix-make-incompatibility.patch
		${FILESDIR}/Bugfix-enable-sec-extension-for-omap3-rom-asm.patch
		${FILESDIR}/no-crosscompile.patch
		${FILESDIR}/backout-omap-extraversion.patch
	"
	kernel-2_src_unpack
	cd "${S}"
	rm -r .gitignore .mailmap
	sed 's/^\(EXTRAVERSION = \).*$/\1'"${EXTRAVERSION}"'/' -i Makefile
}

pkg_postinst() {
	if use fcam; then
		einfo "Fcam requires setting sysctl kernel.sched_rt_runtime_us to -1"
		einfo "To do this now, run: sysctl -w kernel.sched_rt_runtime_us=-1"
		einfo "To set it at boot, append /etc/sysctl.conf with:"
		einfo "    kernel.sched_rt_runtime_us = -1"
	fi
}
