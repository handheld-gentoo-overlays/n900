# Distributed under the terms of the GNU General Public License v2
# $Header$

EAPI=2

K_SECURITY_UNSUPPORTED=1
K_NOSETEXTRAVERSION=1
ETYPE="sources"
inherit eutils versionator

CKV="$(get_version_component_range 1-3 ${OKV})"
MEEGO_GIT_TREE='77e9204e7f8162fe05b6a0c81a59a34306ee08c6'
MY_MEEGO_TREE="meego-os-base-$MEEGO_GIT_TREE.tar.gz"
MEEGO_PATCHES="
	linux-2.6-build-nonintconfig.patch
	
	linux-2.6.35-ac-2010-08-04.patch
	linux-2.6.35-ac-pending.patch
	linux-2.6.35-aava-firmware-workaround.patch
	linux-2.6.35-aava-firmware-workaround-wifi.patch
	linux-2.6.35-make-gma600-work-on-IA.patch
	
	linux-2.6-Hacks-for-Nokia-N900.patch
	linux-2.6.36-Introduce-and-enable-tsc2005-driver.patch
	linux-2.6-SGX-PVR-driver-for-N900.patch
	linux-2.6-Bluetooth-Support-for-n900-bluetooth-hardware.patch
	linux-2.6-mfd-twl4030-Driver-for-twl4030-madc-module.patch
	linux-2.6.36-omap-rx51-Platform-support-for-tsl2563-ALS.patch
	linux-2.6.36-omap-rx51-Platform-support-for-lis3lv02d-acceleromet.patch
	linux-2.6.36-FM-TX-headphone-TV-out-and-basic-jack-detection-supp.patch
	linux-2.6-Earpiece-and-headset-support-for-N900.patch
	linux-2.6.36-wl1251-Use-MODULE_ALIAS-macro-at-correct-postion-for.patch
	linux-2.6-n900-modem-support.patch
	
	linux-2.6.29-silence-acer-message.patch
	linux-2.6.34-rt2860-no-debug.patch
	linux-2.6.33-rt2860-1-2.patch
	linux-2.6.33-rt2860-2-2.patch
	
	linux-2.6.29-dont-wait-for-mouse.patch
	linux-2.6.29-sreadahead.patch
	
	linux-2.6-usb-uvc-autosuspend.patch
	linux-2.6-usb-bt-autosuspend.patch
	
	linux-2.6.33-vfs-tracepoints.patch
	linux-2.6.33-ahci-alpm-accounting.patch
	linux-2.6.35-rc4-annotate-device-pm.patch
	
	linux-2.6.35-slab-timer.patch
	
	linux-2.6.35-dont-skew-the-tick.patch
	
	linux-2.6.35-intel-idle.patch
	
	linux-2.6.34-multi-touch-input-driver-for-event-devices.patch
	linux-2.6.34-enable-hid-dg-contact-count-stantum-and-cando-touch-drivers.patch
	linux-2.6.35-fatal-signals.patch
"
for pf in ${MEEGO_PATCHES}; do
	UNIPATCH_LIST_DEFAULT="${UNIPATCH_LIST_DEFAULT} ${WORKDIR}/meego-os-base-kernel-source/patches/$pf"
done
UNIPATCH_STRICTORDER=true
inherit kernel-2
detect_version
EXTRAVERSION="-meego${MEEGO_GIT_TREE:0:7}"
KV="${CKV}${EXTRAVERSION}"
KV_FULL="${KV}"  # append -${PR} if -rN ?
S="${WORKDIR}/linux-${KV_FULL}"

DESCRIPTION="MeeGo kernel based on Linux"
HOMEPAGE="http://www.meego.com"
	SRC_URI="${KERNEL_URI}
	http://meego.gitorious.org/meego-os-base/kernel-source/archive-tarball/$MEEGO_GIT_TREE -> ${MY_MEEGO_TREE}
"

KEYWORDS="~arm"
IUSE=""

src_unpack() {
	unpack "${MY_MEEGO_TREE}"
	kernel-2_src_unpack
}
