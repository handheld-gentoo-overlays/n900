# Copyright 2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"

inherit git-2

DESCRIPTION="Telepathy connection manager for cellular calls and SMSs, using oFono as backend"
HOMEPAGE="https://meego.gitorious.org/meego-cellular"
SRC_URI=""
EGIT_REPO_URI="git://gitorious.org/meego-cellular/${PN}.git"
EGIT_COMMIT='67d7133e892c032ff4bb1791cb69c650cd41ecf6'

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~arm"
IUSE="debug"

RDEPEND="
	dev-libs/glib:2
	>=dev-libs/check-0.9.4
	>=sys-apps/dbus-0.60
	>=dev-libs/dbus-glib-0.88
	>=net-libs/telepathy-glib-0.11.14
	dev-libs/libxslt
	<dev-lang/python-3
"
DEPEND="${RDEPEND}
	dev-util/pkgconfig
"

src_prepare() {
	./autogen.sh --no-configure
}

src_configure() {
	local myeconfargs=(
		$(use_enable debug)
		--disable-static
		--enable-shared
	)
	econf "${econfargs[@]}"
}
