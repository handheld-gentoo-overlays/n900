# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

EAPI=4

inherit eutils

SRC_URI=""
KEYWORDS="arm"
SLOT="0"
LICENSE="BSD"
HOMEPAGE="http://luke.dashjr.org/programs/gentoo-n900/"
IUSE="+n900"
REQUIRED_USE="n900"

S="${WORKDIR}"

src_install() {
	insinto /
	doins "${FILESDIR}/linuxrc"
}

pkg_postinst() {
	# Shouldn't install a .keep here, in case it's mounted
	mkdir --mode=000 -p /mnt/maemo
}

pkg_postrm() {
	[ -n "$REPLACED_BY_VERSION" ] ||
	ewarn "/mnt/maemo has NOT been removed"
}
