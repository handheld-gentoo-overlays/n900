# Copyright 2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"

inherit git-2

DESCRIPTION="PhoNet utilities"
HOMEPAGE="https://meego.gitorious.org/meego-cellular"
SRC_URI=""
EGIT_REPO_URI="git://gitorious.org/meego-cellular/${PN}.git"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""
IUSE=""

RDEPEND=""
DEPEND=""

src_prepare() {
	# remove debian-based rc TODO
	true
}

src_install() {
	emake DESTDIR="${D}" prefix="/usr" install
	rm -r "${D}/usr/lib" "${D}/lib"
	dodoc README AUTHORS
}
