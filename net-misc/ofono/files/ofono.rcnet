ofono_depend() {
	before interface
}

_config_vars="$_config_vars modem context_type context_apn context_username context_password"

ofono_modem_state() {
	python - "$@" <<\EOF
import dbus
import sys
bus = dbus.SystemBus()
path = sys.argv[1]
modem = dbus.Interface(bus.get_object('org.ofono', path), 'org.ofono.Modem')
props = modem.GetProperties()
if props['Online']:
	sys.exit(0)
elif props['Powered']:
	sys.exit(1)
sys.exit(2)
EOF
}

ofono_find_context() {
	python - "$@" <<\EOF
import dbus
import sys
iface = sys.argv[1]
bus = dbus.SystemBus()
manager = dbus.Interface(bus.get_object('org.ofono', '/'), 'org.ofono.Manager')
props = manager.GetProperties()
for path in props['Modems']:
	connmgr = dbus.Interface(bus.get_object('org.ofono', path), 'org.ofono.DataConnectionManager')
	props = connmgr.GetProperties()
	for path in props["PrimaryContexts"]:
		context = dbus.Interface(bus.get_object('org.ofono', path), 'org.ofono.PrimaryDataContext')
		props = context.GetProperties()
		if (props['Name'] == iface) or (props.has_key('Settings') and props['Settings'].has_key('Interface') and props['Settings']['Interface'] == iface):
			print path
			sys.exit(0)
sys.exit(1)
EOF
}

ofono_find_or_create_context() {
	local iface="$1" modem="$2"
	ofono_find_context "$iface" && return
	echo $(
		dbus-send \
			--system \
			--dest=org.ofono \
			--type=method_call \
			--print-reply=literal \
			"$modem" \
			org.ofono.DataConnectionManager.CreateContext \
			string:"$IFACE" \
			string:"$context_type"
	)
}

ofono_set_property() {
	local objpath="$1" type="$2" prop="$3" value="$4"
	dbus-send \
		--system \
		--dest=org.ofono \
		--type=method_call \
		--print-reply \
		"$objpath" \
		"org.ofono.$type.SetProperty" \
		string:"$prop" \
		variant:"$value" \
		>/dev/null
}

ofono_set_context_property() {
	local context="$1" prop="$2" value="$3"
	ofono_set_property "$context" PrimaryDataContext "$prop" "$value"
}

ofono_sh_context_settings() {
	local context="$1"
	python - "$context" <<\EOF
import dbus
import sys
bus = dbus.SystemBus()
path = sys.argv[1]
context = dbus.Interface(bus.get_object('org.ofono', path), 'org.ofono.PrimaryDataContext')
props = context.GetProperties()
settings = props['Settings']
for key in settings.keys():
	v = settings[key]
	if key == 'DomainNameServers':
		v = ' '.join(v)
	v = v.replace('\\', '\\\\')
	print "local s_%s='%s'" % (key, v)
EOF
}

ofono_pre_start() {
	[ "${IFACE:0:4}" == 'gprs' ] || return 0
	local context context_apn context_password context_type context_username modem modemstate
	
	eval context_type=\"\$context_type_${IFVAR}\"
	[ -n "$context_type" ] || context_type=internet
	eval context_apn=\"\$context_apn_${IFVAR}\"
	if [ -z "$context_apn" ]; then
		eerror "context_apn_$IFVAR is not set!"
		return 1
	fi
	eval context_username=\"\$context_username_${IFVAR}\"
	eval context_password=\"\$context_password_${IFVAR}\"
	eval modem=\"/\$modem_${IFVAR}\"
	
	ofono_modem_state "$modem"
	modemstate="$?"
	if [ "$modemstate" -gt 1 ]; then
		ebegin "Powering on modem '$modem'"
		if ! ofono_set_property "$modem" Modem Powered boolean:true; then
			eend 1
			return 1
		fi
		# Wait for modem to startup
		# FIXME: this should actually wait on the state
		sleep 5
	fi
	if [ "$modemstate" -gt 0 ]; then
		ebegin "Setting modem '$modem' online"
		if ! ofono_set_property "$modem" Modem Online boolean:true; then
			eend 1
			return 1
		fi
		# Wait for modem to get online
		# FIXME: this should actually wait on the state
		sleep 5
	fi
	ebegin "Activating context"
	context="$(ofono_find_or_create_context "$IFACE" "$modem")"
	ofono_set_context_property "$context" AccessPointName string:"$context_apn"
	ofono_set_context_property "$context" Username string:"$context_username"
	ofono_set_context_property "$context" Password string:"$context_password"
	ofono_set_property "$modem" DataConnectionManager Powered boolean:true
	ofono_set_context_property "$context" Active boolean:true
	eval "$(ofono_sh_context_settings "$context")"
	if [ "$s_Interface" != "$IFACE" ]; then
		eend 1
		eerror "Created interface $s_Interface instead of $IFACE"
		# FIXME: kill & destroy bad context
		return 1
	fi
	
	case "$s_Method" in
	dhcp)
		local config
		eval config=\"\$config_$IFVAR\"
		grep -q 'dhcp' <<<"$config" ||
			eval config_$IFVAR=\"dhcp \$config_$IFVAR\"
		;;
	static)
		local nodns nogateway opts
		eval opts=\$dhcp_${IFVAR}
		for opt in ${opts}; do
			case "$opt" in
			nodns)
				nodns=1
				;;
			nogateway)
				nogateway=1
				;;
			esac
		done
		
		local config
		eval config=\"\$config_$IFVAR\"
		perl -e 'exit shift=~m/\Q'"$s_Address"'\E/' "$config" &&
			eval config_$IFVAR=\"$s_Address \$config_$IFVAR\"
		
		if [[ -n "$s_DomainNameServers" && -z "$nodns" ]]; then
			eval dns_servers_$IFVAR=\"\$dns_servers_$IFVAR $s_DomainNameServers\"
		fi
		
		if [ -z "$nogateway" ]; then
			[ -n "$s_Gateway" ] && s_Gateway=" via $s_Gateway"
			local my_metric
			eval my_metric=\"\$metric_$IFVAR\"
			[ -n "$my_metric" ] || my_metric="$metric"
			[ -n "$my_metric" ] || my_metric=4000
			local routes="$(_get_array "routes_$IFVAR")
default$s_Gateway metric $my_metric"
			eval routes_$IFVAR=\"\$routes\"
		fi
		
		;;
	esac
	_up
}

ofono_post_stop() {
	[ "${IFACE:0:4}" == 'gprs' ] || return 0
	context="$(ofono_find_context "$IFACE")"
	if [ -z "$context" ]; then
		eerror "Cannot find context for $IFACE"
		return 1
	fi
	ofono_set_context_property "$context" Active boolean:false
}
